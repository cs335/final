CS335A - Assignment4

GROUP-13

Piyush Sneh Tirkey, 13477

Sahil Grover, 13601

Sandipan Mandal, 13616

Atanu Chakraborty, 13819

*Our 'T' is

	php      	   x86

       	  python

----------------------------------------------------------------------------------------
BRIEF OVERVIEW
-----------------
Features Implemented :-

1. LOOPS:- for, while (used backPatching)

2. CONDITIONALS	 :- if/else (used backPatching)

3. RECURSIVE FUNCTIONS

4. BREAK/CONTINUE

5. Check if a called Function Exists

6. Check Number of arguments in Function Call and Definition

7. SCOPE


----------------------------------------------------------------------------------------
BUILD INSTRUCTIONS
-------------------
1. The test cases are in test directory.

2. To build testi.php - 

	$cd asgn4/bin

	$./script.sh testi.php

