#! /usr/bin/python
import sys


OPdict = {	'+' : 0 , '-' : 1 , '*' : 2 , '/' : 3 , '<=' : 4 , '>=' : 5 , '==': 6 , '<':7 ,'>':8, '!=':9,'%': 10}
mapOPtostring = [ "addl" , "subl" , "imull" , "divl"]
mapToConditions = { 4:"jle" , 5:"jge" , 6 : "je" , 7 : "jl" , 8:"jg" , 9 :"jne"}
stack = [ "%eax" , "%ebx" , "%ecx" ,"%edx" , "%edi" ,"%esi"]
register = { }
OPERATOR = 0
EQUATION = 1
CALL = 2
IFGOTO = 3
LABEL = 4
RETURN = 5
UNCONDITIONAL = 6
MEMORY = 7
REGISTER = 9
EXIT = 10
PRINT = 11
SCAN = 12
EQUATIONLEFT = 13
EQUATIONRIGHT = 14
ARRAY = 15
RETVAL = 16

globalMemoryAllocation = 0
globalLineNumber = 1

def fprint(s):
	global globalLineNumber
	globalLineNumber = globalLineNumber + 1 
	print s
	return


class SymtabEntry:
	lexeme = ""		# lexeme pos
	alive = False
	reg = 0
	Isconst = False
	Isfunc=False
	def __init__(self, word , flag = False):
		self.lexeme = word 
		self.Isfunc = flag
		if word.lstrip('+-').isdigit():
			#print self.lexeme
			self.Isconst = True
		elif not flag:
			fprint(word + ":")
			fprint("\t" + ".long 0")
		
SymtabEntryDictionary = { }


def maplexemeToSymtabEmtry(word,flag=False):
	if SymtabEntryDictionary.has_key(word) is False:
		SymtabEntryDictionary[word] = SymtabEntry(word,flag)
	return SymtabEntryDictionary[word]

class Instruction3AC:
	InstructionType = -1
	operator = -1
	leader = False
	target = ""
	out = None
	in1 = None
	in2 = None
	lineNumber = 0
	def __init__ (self,line):
		#print line
		line = line.split(",")
		#print len(line)
		for i in range(0,len(line)):
			line[i]=line[i].strip(' ')
		#print line[1]
		if OPdict.has_key(line[1]) :
			self.InstructionType = OPERATOR
			self.operator = OPdict.get(line[1])
			self.out = maplexemeToSymtabEmtry(line[2])
			self.in1 = maplexemeToSymtabEmtry(line[3])
			self.in2 = maplexemeToSymtabEmtry(line[4])
		elif line[1] == "=":
		#	print "hi"
			self.InstructionType = EQUATION
			self.operator = OPdict.get(line[1])
			w = line[2].split("[")
			if len(w)>1:
				self.InstructionType = EQUATIONLEFT
				self.in1 = maplexemeToSymtabEmtry((w[1].strip("]"))[0])
				self.out = maplexemeToSymtabEmtry(w[0])
				self.in2 = maplexemeToSymtabEmtry(line[3])
			else:
				w = line[3].split("[")
				if len(w)>1:
					self.InstructionType = EQUATIONRIGHT
					self.out = maplexemeToSymtabEmtry(line[2])
					self.in1 = maplexemeToSymtabEmtry(w[0])
					self.in2 = maplexemeToSymtabEmtry((w[1].strip("]") )[0])
				else:
					self.InstructionType = EQUATION
					self.out = maplexemeToSymtabEmtry(line[2])
					self.in1 = maplexemeToSymtabEmtry(line[3])
		elif line[1] == "call":
			self.InstructionType = CALL
			self.target = line[2]
		elif line[1] == "ifgoto":
			self.InstructionType = IFGOTO
			self.operator = OPdict.get(line[2])
			self.in1 = maplexemeToSymtabEmtry(line[3])
			self.in2 = maplexemeToSymtabEmtry(line[4])
			if self.in1.Isconst :
				self.in1 , self.in2 = self.in2 , self.in1
			if line[5].isdigit():
				self.target = int(line[5])
			else:
				self.target = line[5]
		elif line[1] == 'label':
			self.InstructionType = LABEL
			self.in1 = maplexemeToSymtabEmtry(line[2],True)
		elif line[1] == 'ret':
			self.InstructionType = RETURN
			if len(line)>2:
				self.in1 = maplexemeToSymtabEmtry(line[2])
		elif line[1] == 'exit':
			self.InstructionType = EXIT
		elif line[1] == 'print':
			self.InstructionType = PRINT
			self.in1 = maplexemeToSymtabEmtry(line[2])
		elif line[1] == 'scan':
			self.InstructionType = SCAN
			self.in1 = maplexemeToSymtabEmtry(line[2])
		elif line[1] == 'array':
			self.InstructionType = ARRAY
			self.in1 = maplexemeToSymtabEmtry(line[2],True)
			self.in1.Isfunc = False			# Not a function
			fprint(line[2] + ":\n\t .skip " + line[3])
		elif line[1] == 'retval':			
			self.InstructionType = RETVAL   
			self.in1 = maplexemeToSymtabEmtry(line[2])   



ir = []
filename = str(sys.argv[1])
inputfile = open(filename)
code = inputfile.read()
code = code.split("\n")

fprint(".section .data")
fprint("formatOutput:\t .string \"%ld\\n\\0\"")
fprint("formatInput:\t .string \"%ld\\0\"")
fprint("wasteMemory:")
fprint("\t.long 0")
fprint("newline:        #For printing a new line")
fprint('.ascii \"\\n\"')

fprint("printNum:")
fprint("	.long 1234567	  	#Number to print")	

for line in code:
	ir.append(Instruction3AC(line))
#	print str(ir[len(ir)-1].InstructionType) + " " + str(ir[len(ir)-1].operator) +"\n"
fprint( ".section .text")
fprint(".globl _start")
fprint(".type print, @function")
fprint("_start:")

for i in range(len(ir)):
	if i==0:
		ir[0].leader = True
	if ir[i].InstructionType == IFGOTO:
		ir[(int)(ir[i].target)-1].leader = True
		if(i+1<len(ir)):
			ir[i+1].leader = True
	elif ir[i].InstructionType == CALL:
		if(i+1<len(ir)):
			ir[i+1].leader = True
	elif ir[i].InstructionType == LABEL:
		ir[i].leader = True
	elif ir[i].InstructionType == PRINT or ir[i].InstructionType == SCAN :
		if(i+1<len(ir)):
			ir[i+1].leader = True

	# elif ir[i].InstructionType == RETURN:
	# 	ir[i].leader = True

IGNORESTATEMENTS = [ CALL , LABEL , RETURN , EXIT]
INF = 100000

def getFreeRegister(NextUseDictionary):
	if len(stack)>0:
		r = stack[len(stack)-1]
		del stack[-1]
		return r
	s = None
	dis = 0
	for key,item in register.iteritems():
		if dis<NextUseDictionary[key]:
			s = key
			dis	= NextUseDictionary[key]
	fprint ("movl " + register[s] + "," + s.lexeme)
	r = register[s]
	del register[s]
	return r

def getreg(var , typeOut , NextUseDictionary, left ,var1 = None ):
	if var.Isconst:
		return " $"+str(var.lexeme)
	if typeOut == UNCONDITIONAL:
		if register.has_key(var):
			return register[var]
		else:
			return var.lexeme 
	if typeOut == MEMORY:
		return  var.lexeme
	if typeOut == REGISTER:
		if register.has_key(var):
			return register[var]
		if register.has_key(var1) and NextUseDictionary[var1]==INF:
			register[var] = register[var1]
			fprint("movl " + register[var1] + "," + var1.lexeme)
			del register[var1]
			return register[var]
		
		register[var] = getFreeRegister(NextUseDictionary)
		if left is False:
			fprint ("movl " + var.lexeme + "," + register[var])
		return register[var]


def moveCondition(in1 , out, s, NextUseDictionary):
	if not register.has_key(in1) and not register.has_key(out) and not in1.Isconst:
		s = getreg(out,REGISTER,NextUseDictionary,False)
	return s

def exitingLine(ob,NextUseDictionary):
	if ob.out is not None and NextUseDictionary.has_key(ob.out) and NextUseDictionary[ob.out]==INF and register.has_key(ob.out):
		stack.append(register[ob.out])
		fprint ("movl " + register[ob.out] +"," + ob.out.lexeme)
		del register[ob.out]
	if ob.in1 is not None and NextUseDictionary.has_key(ob.in1) and NextUseDictionary[ob.in1]==INF and register.has_key(ob.in1):
		stack.append(register[ob.in1])
		fprint ("movl " + register[ob.in1] +"," + ob.in1.lexeme)
		del register[ob.in1]
	if ob.in2 is not None and NextUseDictionary.has_key(ob.in2) and NextUseDictionary[ob.in2]==INF and register.has_key(ob.in2):
		stack.append(register[ob.in2])
		fprint ("movl " + register[ob.in2] +"," + ob.in2.lexeme)
		del register[ob.in2]


def GenerateInstruction(NextUseDictionary , line):
	ob = ir[line]
	#print ob.InstructionType
	if ob.InstructionType == CALL:
		exitingLine(ob,NextUseDictionary)
		fprint ("call " + str(ob.target))
	elif ob.InstructionType == RETVAL:           
		r = getreg(ob.in1,UNCONDITIONAL,NextUseDictionary,False)      
		fprint( "movl %eax," + r )                 
	elif ob.InstructionType == EXIT:
		fprint ("movl $1, %eax")
		fprint ("movl $0, %ebx")
		fprint ("int $0x80")
		return
	elif ob.InstructionType == PRINT:
		r=""
		if not ob.in1.Isconst: 
			r = getreg(ob.in1,REGISTER,NextUseDictionary,False)
		else:
			r= "$"+ob.in1.lexeme	
		#fprint ("movl " + r + ", printNum")
		fprint("pushl " + r)
		fprint("pushl $formatOutput")

		exitingLine(ob,NextUseDictionary)
		fprint ("call printf")
	elif ob.InstructionType == SCAN:
		exitingLine(ob,NextUseDictionary)
		r = getreg(ob.in1,MEMORY,NextUseDictionary,False)
		fprint("pushl $" + r )
		fprint("pushl $formatInput")
		fprint("call scanf")
	elif ob.InstructionType == LABEL:
		fprint (".type " + str(ob.in1.lexeme) + ", @function") 
		fprint (str(ob.in1.lexeme) +":\t")
		fprint ("pushl %ebp")
		fprint ("movl %esp, %ebp")
	elif ob.InstructionType == RETURN:
		# for key,val in register.iteritems():
		# 	if val=="%eax" and key is not ob.in1:
		# 		print "movl %eax, " + ob.in1.lexeme
		# 		del register[key]
		# 		break 
		if ob.in1 is not None and (not register.has_key(ob.in1) or register[ob.in1]!="%eax") :
			fprint ("movl " + getreg(ob.in1,UNCONDITIONAL,NextUseDictionary,False) + ", %eax ")
		if register.has_key(ob.in1):
			fprint ("movl %eax, " + ob.in1.lexeme)
			del register[ob.in1] 
		fprint ("movl %ebp, %esp")
		fprint ("popl %ebp")
		fprint ("ret")
	elif ob.InstructionType == EQUATIONLEFT:
	
		r = getreg(ob.in1,REGISTER,NextUseDictionary,False)
		if ob.in1.Isconst is True:
			r2 = r
			r = getFreeRegister(NextUseDictionary)
			fprint("movl " + r2 + ", " + r)
			stack.append(r)
		s = getreg(ob.in2,REGISTER,NextUseDictionary,False)
		fprint( "movl " + s + "," + ob.out.lexeme + "(," + r + ",4 )")
		exitingLine(ob,NextUseDictionary)
	
	elif ob.InstructionType == EQUATIONRIGHT:

		r = getreg(ob.out,REGISTER,NextUseDictionary,False)
		s = getreg(ob.in2,REGISTER,NextUseDictionary,False)
		if ob.in2.Isconst is True:
			r2 = s
			s = getFreeRegister(NextUseDictionary)
			fprint("movl " + r2 + ", " + s)
			stack.append(s)
		fprint( "movl " + ob.in1.lexeme + "(," + s + ",4 ), " + r)
		exitingLine(ob,NextUseDictionary)
	
	elif ob.InstructionType == EQUATION:		
		s = ""
		#print NextUseDictionary[ob.out]
		r = getreg(ob.in1,UNCONDITIONAL,NextUseDictionary,False)
		#print NextUseDictionary[ob.out]
		if NextUseDictionary[ob.out] == INF:
			s = getreg(ob.out,MEMORY,NextUseDictionary,True)
		else:
			s = getreg(ob.out,REGISTER,NextUseDictionary,True,ob.in1)
		if r != s:
			s = moveCondition(ob.in1,ob.out , s,NextUseDictionary)
			fprint("movl " + r + ", " + s)
		exitingLine(ob,NextUseDictionary)
	elif ob.InstructionType == IFGOTO:
		r = getreg(ob.in1,REGISTER,NextUseDictionary,False)
		s = getreg(ob.in2,UNCONDITIONAL,NextUseDictionary,False)
		if ob.in1.Isconst:
			s = getFreeRegister(NextUseDictionary)
			fprint("movl " + r + ", " + s)
			r = s
		fprint("cmpl " + s +", " + r)
		exitingLine(ob,NextUseDictionary)
		fprint(mapToConditions[ob.operator] + " " + str("ABC" + str(ob.target-1)))

	elif ob.InstructionType == OPERATOR and ob.operator != OPdict['/'] and ob.operator != OPdict['%'] :
		r1 = getreg(ob.in1,UNCONDITIONAL,NextUseDictionary,False)
		r2 = getreg(ob.in2,UNCONDITIONAL,NextUseDictionary,False)		
		s = getreg(ob.out,REGISTER,NextUseDictionary,True,ob.in1)
	#	print r1 +" " + r2 + " " + s
		if r1!=s:
			fprint ("movl " + r1 + ", " + s)
		fprint (mapOPtostring[ob.operator] + " " + r2 + ", " + s)
		exitingLine(ob,NextUseDictionary)
	elif ob.InstructionType == OPERATOR:

		

		for key,val in register.iteritems():
			if val=="%edx":
				fprint ("movl %edx, " + key.lexeme)
				del register[key]
				break
		for key,val in register.iteritems():
			if val=="%eax":
				if ob.in1 not in register or  (register[ob.in1] is not "%eax"):
					fprint ("movl %eax, " + key.lexeme)
					del register[key]
					break
		fprint ("movl $0, %edx")
		r = getreg(ob.in1,UNCONDITIONAL,NextUseDictionary,False)

		if ob.in1 not in register or  (register[ob.in1] is not "%eax"):
			fprint ("movl " + r + ", %eax")

		if register.has_key(ob.out) and register[ob.out]!="%eax" and register[ob.out]!="%edx":
			stack.append(register[ob.out])

		if ob.operator is OPdict['/']:
			register[ob.out] = "%eax"
		elif ob.operator is OPdict['%'] :
		 	register[ob.out] = "%edx"


		s = getreg(ob.in2,UNCONDITIONAL,NextUseDictionary,False)
		if ob.in2.Isconst:
			fprint ("movl " + s + ", wasteMemory")
			s = "wasteMemory" 
		fprint ("divl " + s)
		if ob.operator is OPdict['/']:
			stack.append("%edx")
		elif ob.operator is OPdict['%'] :
		 	stack.append("%eax")
		
		exitingLine(ob,NextUseDictionary)



def RegisterAllocation(line1 , line2):
	changeInNextUseArray = []
	NextUseDictionary = { }
	line = line2
	while line>=line1:
		new = { }
		if ir[line].InstructionType not in IGNORESTATEMENTS:
			if ir[line].out is not None and NextUseDictionary.has_key(ir[line].out):
				new[ir[line].out] = NextUseDictionary[ir[line].out]
			else:
				new[ir[line].out] = INF
			NextUseDictionary[ir[line].out] = INF
			if ir[line].in1 is not None and ir[line].out!=ir[line].in1 and not ir[line].in1.Isconst:
				if NextUseDictionary.has_key(ir[line].in1):
					new[ir[line].in1] = NextUseDictionary[ir[line].in1]
				else:
					new[ir[line].in1] = INF
				NextUseDictionary[ir[line].in1]=line
			if ir[line].in2 is not None:
				if ir[line].in1 != ir[line].in2 and ir[line].in2 != ir[line].out and not ir[line].in2.Isconst:
					if NextUseDictionary.has_key(ir[line].in2):
						new[ir[line].in2] = NextUseDictionary[ir[line].in2]
					else:
						new[ir[line].in2] = INF
					NextUseDictionary[ir[line].in2]=line
		if ir[line].InstructionType == RETURN:
			if ir[line].in1 is not None and not ir[line].in1.Isconst:
				if NextUseDictionary.has_key(ir[line].in1):
					new[ir[line].in1] = NextUseDictionary[ir[line].in1]
				else:
					new[ir[line].in1] = INF
				NextUseDictionary[ir[line].in1]=line
		line = line - 1
		changeInNextUseArray.append(new)
	#	for key,val in NextUseDictionary.iteritems():
	#		print key.lexeme + " " + str(val)
	line = line1
	#print str(line1) + " " + str(line2)
	changeInNextUseArray.reverse()
	while line<=line2:
		for v,p in changeInNextUseArray[line-line1].iteritems():
			NextUseDictionary[v] = p
		if line==line1:
			ir[line].lineNumber = "ABC" + str(line)
			fprint(ir[line].lineNumber + ":")
		GenerateInstruction(NextUseDictionary,line)
		line = line + 1

index = 0
count11=1
for i in range(len(ir)):
	if i!=0 and ir[i].leader:
		count11 = count11 + 1
		#print str(index) + " " + str(i)
		RegisterAllocation(index,i-1)
		index = i
RegisterAllocation(index,len(ir)-1)

