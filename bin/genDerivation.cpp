#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define pii pair<int,int>
#define pll pair<ll,ll>
#define pdd pair<double,double>
#define X first
#define Y second
#define REP(i,a) for(int i=0;i<a;++i)
#define REPP(i,a,b) for(int i=a;i<b;++i)
#define FILL(a,x) memset(a,x,sizeof(a))
#define	foreach( gg,itit )	for( typeof(gg.begin()) itit=gg.begin();itit!=gg.end();itit++ )
#define	mp make_pair
#define	pb push_back
#define sz(a) int((a).size())
#define all(a)  a.begin(), a.end()
#define	debug(ccc)	cout << #ccc << " = " << ccc << endl;
#define present(c,x) ((c).find(x) != (c).end())
const double eps = 1e-8;
#define EQ(a,b) (fabs((a)-(b))<eps)
inline int max(int a,int b){return a<b?b:a;}
inline int min(int a,int b){return a>b?b:a;}
inline ll max(ll a,ll b){return a<b?b:a;}
inline ll min(ll a,ll b){return a>b?b:a;}
const int mod = 1e9+7;
const int N = 1e6+10;
const ll inf = 1e18;

ll power(ll a,ll n){
	if(n==0){
		return 1;
	}
	ll b = power(a,n/2);
	b = b*b%mod;
	if(n%2) b= b*a%mod;
	return b;
}

int add(int a,int b){ return (a+b)%mod;}
int mul(int a,int b){ return (ll)a*b%mod;}


vector < vector < string > > G;

int main(int argc, char *argv[]){
  	freopen(argv[1],"r",stdin);
  	freopen(argv[2],"w",stdout);
	// printf("%s\n", argv[1]);
	vector < string > v;
	v.pb("start");
	G.pb(v);
	cout<<"<!DOCTYPE html>\n<html>\n<head>\n<title>";
	cout<<argv[2];
	cout<<"</title>\n</head>\n<body bgcolor=\"#E6E6FA\">\n<h1> Rightmost Derivation </h1>\n";
	cout<<"<ol>\n";
	while(true){
		v.clear();
		int f=0;
		while(true){
			string s;cin>>s;
			if(s=="-1"){
				f=1;break;
			}
			if(s=="0"){	G.pb(v);break;}
			v.pb(s);
		}
		if(f)	break;
	}
	//reverse(all(G));
	vector < string > A = G[0];
	// REP(i,A.size())	cout<<A[i]<<" ";cout<<endl;
	REPP(i,1,G.size()){
		int indx = -1;
		REP(j,A.size()){
			if(G[i][0]==A[j])	indx = j;
		}
		assert(indx!=-1);
		cout<<"<li>\n"; 
		REP(j,indx) cout<<A[j]<<" ";cout<<endl<<endl;
		cout<<"<font size=\"4\" color=\"red\"><u>"<<A[indx]<<"</u>"<<"</font>"<<endl;
		REPP(j,indx+1,A.size())	cout<<A[j]<<" ";cout<<endl<<endl;
		vector < string > B;
		REP(j,indx)	B.pb(A[j]);
		REPP(j,1,G[i].size())	B.pb(G[i][j]);
		REPP(j,indx+1,A.size())	B.pb(A[j]);
		A = B;
		cout<<"<br><br>"<<endl;
		
	}
	cout<<"<li>\n"; 
	REP(j,A.size())	cout<<A[j]<<" ";cout<<endl<<endl;
	cout<<"</ol>\n";
	cout<<"</body>\n</html>";
		
	return 0;
}
