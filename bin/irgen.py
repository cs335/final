#! /usr/bin/python

import ply.yacc as yacc
import sys
import make_graph
import essentials
import symbolTable as sT

# import lex

# sys.path.append('/home/sandipan/Desktop/sem6/CS335A/assignment/asgn1/bin/')


symTable = sT.SymbolTable()
classDict = { }
HashDictClass = { }
insideClass = 0
currentClass = ""
curflag = 0
curlexeme =""


import lexer

tokens = lexer.tokens

for arg in sys.argv:
	filename = arg

openfile = open(filename)

data = openfile.read()

lineNo = 0
lvl = 0
loopinglvl = 0
def inclLineNo():
	global lineNo
	lineNo += 1

functiondefinition = [ ]

precedence = (
	('nonassoc', 'EQUAL', 'NOTEQUAL', 'GREATER', 'GREATEREQ', 'LESSER', 'LESSEREQ'),
    ('right', 'ASSIGN', 'PLUS2', 'MULT2', 'DIVIDE2', 'MINUS2', 'MOD2'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULT', 'DIVIDE','MOD'),
    ('right','POWER'),
    ('left', 'ARROW'),
	('nonassoc', 'LPAREN')
)



def p_start(p):
	'''start : START statement_list1
	| error'''

	Hash = symTable.get_Hash_Table()
	for f in functiondefinition:
		if not Hash.has_key(f[0]):
			print "error , function " + f[0] + " not defined"
		else:
			if Hash[f[0]]["#attributes"] != f[1]:
				print "error , function " + f[0] +" has wrong #arguments"

	print "\nSymbol Table For Global Scope"
	print Hash
	# if len(p) == 3:
	# 	p[0] = p[1] + p[2]
	# else:
	# 	p[0] = p[1]
	#print p.slice
	##print "start -> " + p[0]
	inclLineNo()


def p_statement_list1(p):
	'''statement_list1 : statement_list2 END
	| error'''
	if len(p)==3:
		if len(p[1]["breakList"])>0:
			print "error: extra break statements"
		if len(p[1]["continueList"])>0:
			print "error: extra continue statements" 
	# if len(p) == 3:
	# 	p[0] = p[1] + p[2]
	# else:
	# 	p[0] = p[1]
	#print p.slice
	##print "statement_list1 -> " + p[0]
	inclLineNo()

def p_statement_list2(p):
	'''statement_list2 : statement_list2 m statement
	| statement'''
	essentials.counter = 0

	p[0] = { }
	# print "hi1",p[1]["nextList"]
	if not isinstance(p[1], basestring):
		# print "hi2",p[1]["nextList"]
		if len(p)==4:
			essentials.backPatch(p[1]["nextList"], p[2]["quad"])
			p[0]["nextList"] = p[3]["nextList"] 
			p[0]["breakList"] = p[1]["breakList"] 
			p[0]["continueList"] = p[1]["continueList"] 
			if p[3].has_key("continueList"):
				p[0]["continueList"] = p[0]["continueList"] + p[3]["continueList"] 
			if p[3].has_key("breakList"):
				p[0]["breakList"] = p[0]["breakList"] + p[3]["breakList"]
		else:
			# print "gg"
			# print p[1]["nextList"]
			# essentials.backPatch(p[1]["nextList"], len(essentials.threeAdrCode) )
			p[0]["nextList"] = p[1]["nextList"]
			p[0]["breakList"] = [ ]
			if p[1].has_key("breakList"):
				p[0]["breakList"] = p[1]["breakList"]

			p[0]["continueList"] = [ ]
			if p[1].has_key("continueList"):
				p[0]["continueList"] = p[1]["continueList"]
			
	else:
		# essentials.backPatch(p[1]["nextList"], len(essentials.threeAdrCode) )
		p[0]["nextList"]= [ ]
		p[0]["breakList"] = [ ]
		p[0]["continueList"] = [ ]

	# if len(p)==3:
	# 	p[0] = p[1] + p[2]
	# else:
	# 	p[0] = p[1]
	#print p.slice
	##print "statement_list2 -> " + p[0]
	inclLineNo()

def p_statement(p):
	'''statement : expression_state
	| ifexpression 
	| for_statement
	| while_statement 
	| switch_state
	| function_statement
	| function_call_statement SEMICOLON
	| break 
	| continue
	| return
	| array_declaration SEMICOLON
	| GLOBAL id
	| ECHO expression SEMICOLON
	| SINGLELINECOMMENTS 
	| BLOCKCOMMENTS
	| classasignstatement
	| class_statement
	| error'''
	
	p[0] = { }
	try:
		p[0]["nextList"] = p[1]["nextList"]
#		print "gg",p[1]["nextList"]
	except:
		p[0]["nextList"] = [ ]
	try:
		p[0]["breakList"] = p[1]["breakList"]
	except:
		p[0]["breakList"] = [ ]
	try:
		p[0]["continueList"] = p[1]["continueList"]
	except:
		p[0]["continueList"] = [ ]

	##print "statement -> " + p[0]
	#print p.slice
	inclLineNo()


def p_class_statement(p):
	'''class_statement : CLASS class_name mark1 LBRACE class_statementlist RBRACE
	| error'''

	p[0] = { }
	p[0]["nextList"] = p[5]["nextList"]
	essentials.classthreeAddr.append(["ret"])
	essentials.backPatchClass(p[3]["classNextList"],len(essentials.classthreeAddr))
	global insideClass
	insideClass = 0

def p_class_name(p):
	'''class_name : IDENTIFIER_OTHER '''
	p[0] = { }
	p[0]["lexeme"] = p[1]
	classDict[p[1]] = { }
	classDict[p[1]]["number"] = len(classDict)	
	classDict[p[1]]["mallocList"] = [ ]
	classDict[p[1]]["size"] = 0
	global currentClass
	currentClass = p[1]

def p_mark1(p):
	'''mark1 : 
	'''
	p[0] = { }
	symTable.begin_scope()
	essentials.classthreeAddr.append(["ifgoto","==","classname",len(classDict),str(len(essentials.classthreeAddr)+2)])
	p[0]["classNextList"] = len(essentials.classthreeAddr)
	essentials.classthreeAddr.append(["goto"])
	global insideClass
	if insideClass == 1:
		print "error, Can not define class inside class"
	insideClass = 1



def p_class_statementlist(p):
	'''class_statementlist : classvars class_statementlist
	| classfuncdec class_statementlist
	|	
	| error'''
	p[0] = { }
	if len(p)==1:
		p[0]["nextList"] = [ ]
	elif len(p)==3:
		try:
			p[0]["nextList"] = p[2]["nextList"] + p[1]["nextList"]
		except:
			p[0]["nextList"] = p[2]["nextList"]



def p_classvars(p):
	'''classvars : VAR IDENTIFIER_VARIABLE SEMICOLON
	| PUBLIC IDENTIFIER_VARIABLE SEMICOLON
	| PRIVATE IDENTIFIER_VARIABLE SEMICOLON
	| error'''
	p[0] = { }
	
	if not insideClass:
		print "error, not inside class"
	classDict[currentClass]["size"] += 1
	p[0]["lexeme"] = [p[2],0]
	if p[1]=="private":
		p[0]["lexeme"][1] = 1
	if not HashDictClass.has_key(p[2]):
		HashDictClass[p[2]] = len(HashDictClass)
	x = len(essentials.classthreeAddr)
	essentials.classthreeAddr.append(["ifgoto","!=","classvar",str(HashDictClass[p[2]]), str(x+5) ])
	essentials.classthreeAddr.append(["ifgoto","<","classflag", str(p[0]["lexeme"][1]) , str(x+4) ])
	essentials.classthreeAddr.append(["stack_pos1",str(classDict[currentClass]["size"]),"2"])
	essentials.classthreeAddr.append(["ret"])
	essentials.classthreeAddr.append(["print","error"])



def p_classfuncdec(p):
	'''classfuncdec : function_statement 
	| PRIVATE function_statement
	| PUBLIC function_statement
	| error'''
	p[0] = { }
	if len(p)==2:
		p[0]["nextList"] = p[1]["nextList"]
		p[0]["lexeme"] = [ p[1]["lexeme"] , 0] 
	elif len(p)==3:
		p[0]["nextList"] = p[2]["nextList"]
		p[0]["lexeme"] = [ p[2]["lexeme"] , 0] 		
	if p[1]=="private":
		p[0]["lexeme"][1] = 1
	if not HashDictClass.has_key(p[0]["lexeme"][0]):
		HashDictClass[p[2]] = len(HashDictClass)
	x = len(essentials.classthreeAddr)
	essentials.classthreeAddr.append(["ifgoto","!=","classvar",str(HashDictClass[p[0]["lexeme"][0]]), str(x+5) ])
	essentials.classthreeAddr.append(["ifgoto","<","classflag", str(p[0]["lexeme"][1]) , str(x+4) ])
	essentials.classthreeAddr.append(["call",currentClass+"_"+p[0]["lexeme"][0]])
	essentials.classthreeAddr.append(["ret"])
	essentials.classthreeAddr.append(["print","error"])



def p_classasignstatement(p):
	'''classasignstatement : IDENTIFIER_VARIABLE ASSIGN NEW IDENTIFIER_OTHER LPAREN arg_val RPAREN SEMICOLON
	| IDENTIFIER_VARIABLE ASSIGN NEW IDENTIFIER_OTHER LPAREN RPAREN SEMICOLON
	| classterm ASSIGN NEW IDENTIFIER_OTHER LPAREN arg_val RPAREN SEMICOLON
	| classterm ASSIGN NEW IDENTIFIER_OTHER LPAREN RPAREN SEMICOLON
	| classterm ASSIGN expression
	| error'''

	p[0]= {}
	p[0]["nextList"]=[]
	if len(p)==4:
		p[0]["lexeme"] = p[1]["prevlexeme"]
		essentials.AddToThree(["=",p[0]["lexeme"],p[3]["lexeme"]])
	else:	
		if isinstance(p[1],basestring):
			p[0]["lexeme"] = p[1]
		else :
			p[0]["lexeme"] = p[1]["prevlexeme"]
		if p[4] not in classDict :
			print "error: class not defined"	
		v1 = essentials.getNewTemp()
		symTable.createSym(v1,False)
		classDict[p[4]]["mallocList"].append(len(essentials.threeAdrCode))
		essentials.AddToThree(["new_array",v1])
		essentials.AddToThree(["=",p[0]["lexeme"],v1])
		essentials.AddToThree(['=',v1+"[0]",classDict[p[4]]])

		if lvl>0:
			# print "hi"
			for item,k in symTable.symbol_table.Hash.iteritems():	
				# print "gg" + item
				essentials.AddToThree(["push",item])
				v.append(item)
			for i in range(essentials.maxcounter):
				essentials.AddToThree(["push","var"+str(i)])
				v.append("var"+str(i))
		
		if len(p)==9:
			p[6]["vars"].reverse()
			for lexeme in p[6]["vars"]:
				# v.append(lexeme)
				essentials.AddToThree(["push",lexeme])
		
		essentials.AddToThree(["push",v1])
		
		essentials.AddToThree(["push","0"])
		

		essentials.AddToThree(["=","classname",v1+"[0]"])
		name = p[4]+ "_"+ "__construct"

		if name not in HashDictClass:
			print "error: function not in class"
		else:
			essentials.AddToThree(["=","classvar",HashDictClass[name]])
		
		classflag = 0	
		essentials.AddToThree(["=","classflag",str(classflag)])
		essentials.AddToThree(["call","classFunction"])			



		p[0]["lexeme"] = essentials.getNewTemp()

		essentials.AddToThree(["pop"])


		essentials.AddToThree(["pop"])

		if len(p)==9:		
			for lexeme in p[6]["vars"]:
				essentials.AddToThree(["pop"])
				
		v.reverse()

		for lexeme in v:
			essentials.AddToThree(["pop",lexeme])
	##print "function_call_statement -> " + p[0]
	inclLineNo()


			

def p_classterm(p):
	'''classterm : classterm ARROW IDENTIFIER_VARIABLE
	| classfunctioncall ARROW IDENTIFIER_VARIABLE
	| IDENTIFIER_VARIABLE ARROW IDENTIFIER_VARIABLE
	| THIS ARROW IDENTIFIER_VARIABLE
	|	error'''

	p[0]={}
	p[0]["lexeme"] = essentials.getNewTemp()
	symTable.createSym(p[0]["lexeme"],False)
	if isinstance(p[1],basestring):
		p[1]={ "lexeme":p[1] }
	if not HashDictClass.has_key(p[3]):
		print "error, no variable assigned in given class"
	if len(p)==4:
		essentials.AddToThree(["=","classname",p[1]["lexeme"]+"[0]"])
		essentials.AddToThree(["=","classvar",HashDictClass[p[3]]])		
	
	if not insideClass and p[1]=="$this":
		print "error, wrong THIS statement"
	if p[1]=="$this":
		classflag = 1
	else:
		classflag = 0
	essentials.AddToThree(["=","classflag",classflag])
	essentials.AddToThree(["push","0"])
	essentials.AddToThree(["call","classFunction"])
	v1 = essentials.getNewTemp()
	symTable.createSym(v1,False)
	essentials.AddToThree(["pop",v1])
	p[0]["prevlexeme"] =  p[1]["lexeme"]+"["+v1+"]"
	essentials.AddToThree(["=",p[0]["lexeme"],p[1]["lexeme"]+"["+v1+"]"])


def p_classfunctioncall(p):
	'''classfunctioncall : classfunctioncall clmark ARROW function_call_statement 
	| classterm clmark ARROW function_call_statement 
	| IDENTIFIER_VARIABLE clmark ARROW function_call_statement 
	| THIS clmark ARROW function_call_statement 
	|	error'''

	p[0]={}
	p[0]["lexeme"] = p[4]["lexeme"]
	p[0]["nextList"] =[]
	global curflag
	curflag = 0

def p_clmark(p):
	'''clmark : 
	| error'''
	p[0]={}
	global curflag
	global curlexeme
	curflag = 1

	if isinstance(p[-1],basestring):
			curlexeme = p[-1]
	else:
		curlexeme = p[-1]["lexeme"]	


# def p_datatype2(p):
# 	'''datatype2 : INTEGER
# 	| FLOAT 
# 	| STRING
# 	| NEW IDENTIFIER_OTHER LPAREN arg_val RPAREN
# 	| NEW IDENTIFIER_OTHER LPAREN RPAREN'''
# 	p[0] = { }
# 	p[0]["lexeme"] = str(p[1])
# 	##print "datatype -> " + p[0]
# 	inclLineNo()


def p_id(p):
	'''id : IDENTIFIER_VARIABLE SEMICOLON 
	| IDENTIFIER_VARIABLE SEPARATOR id
	| error'''

	if len(p) == 3:
		p[0] = p[1] + p[2]
	elif len(p) == 4:
		p[0] = p[1] + p[2] + p[3]
	else:
		p[0] = p[1]

	inclLineNo()

def p_switch_state(p):
	'''switch_state : SWITCH LPAREN IDENTIFIER_VARIABLE RPAREN LBRACE switch_case switch_default RBRACE 
	| SWITCH LPAREN IDENTIFIER_VARIABLE RPAREN LBRACE switch_default RBRACE 
	| SWITCH LPAREN IDENTIFIER_VARIABLE RPAREN LBRACE switch_case  RBRACE 
	| SWITCH LPAREN IDENTIFIER_VARIABLE RPAREN LBRACE  RBRACE 
	| error'''

	if(len(p)==9):
		p[0]=p[1] + p[2] + p[3] + p[4] + p[5] + p[6] +p[7] + p[8]
	elif(len(p)==8):
		p[0]=p[1] + p[2] + p[3] + p[4] + p[5] + p[6] +p[7]
	elif len(p)==7:
		p[0]=p[1] + p[2] + p[3] + p[4] + p[5] + p[6] 
	else :
		p[0]=p[1]

	##print "switch_state -> " + p[0] 
	inclLineNo()
	#print p.slice


def p_switch_case(p):
	''' switch_case : CASE datatype_switch COLON  statement_list2 switch_case 
	| CASE datatype_switch COLON  statement_list2
	| error'''

	if(len(p)==6)	:
		p[0]= p[1] + p[2] + p[3] + p[4] + p[5] 
	elif len(p)==5:
		p[0]= p[1] + p[2] + p[3] + p[4] 
	else :
		p[0] = p[1]
	##print "switch_case -> " + p[0]
	inclLineNo()
	#print p.slice
		
def p_switch_default(p):
	'''switch_default : DEFAULT COLON statement_list2
	| error '''
	if len(p) == 4:
		p[0] = p[1] +p[2] +p[3]
	##print "switch_default -> " + p[0]
	else :
		p[0] = p[1]
	inclLineNo()



def p_datatype_switch(p):
	'''datatype_switch : INTEGER 
	|	STRING'''
	p[0]=str(p[1])
	##print "datatype_switch -> " + p[0]
	inclLineNo()

def p_break(p):
	''' break : BREAK SEMICOLON 
	| error'''

	p[0] = { }
	if loopinglvl==0:
		print "error: extra break"
	##print "break -> " + p[0]
	if len(p)==3:
		p[0]["breakList"] = [ len(essentials.threeAdrCode) ]
		essentials.AddToThree(["goto"])
	else:
		p[0]=p[1]
	inclLineNo()

def p_continue(p):
	''' continue : CONTINUE SEMICOLON 
	| error'''
	p[0] = { }
	if loopinglvl==0:
		print "error: extra continue"
	if len(p)==3:
		p[0]["continueList"] = [ len(essentials.threeAdrCode) ]
		essentials.AddToThree(["goto"])
	else:
		p[0]=p[1]
	inclLineNo()

def p_return(p):
	''' return : RETURN SEMICOLON 
	| RETURN expression SEMICOLON 
	| error'''

	p[0] = { }
	if len(p)==4:
		essentials.AddToThree(["stack_pos1",p[2]["lexeme"],"2"])

	essentials.AddToThree(["ret"])
	##print "continue -> " + p[0]
	inclLineNo()

def p_ifexpression(p):
	'''ifexpression : IF LPAREN boolexpression RPAREN m statement_list3 nm elseifexpression 
	| error'''
	
	p[0] = { }
	if len(p) == 9:
		essentials.backPatch(p[3]["trueList"] , p[5]["quad"] )
 		essentials.backPatch(p[3]["falseList"], p[7]["quad"] )
 		p[0]["nextList"] = essentials.mergeList(p[6]["nextList"], p[7]["nextList"])
 		p[0]["nextList"] = essentials.mergeList(p[0]["nextList"], p[8]["nextList"])
		p[0]["breakList"] = p[6]["breakList"] + p[8]["breakList"]
		p[0]["continueList"] = p[6]["continueList"] + p[8]["continueList"]

	##print "ifexpression -> "  + p[0]
	inclLineNo()


def p_nm(p):
	'''nm : '''
	p[0] = { }
	p[0]["nextList"] = [ len(essentials.threeAdrCode)]
	essentials.AddToThree(["goto"])
	p[0]["quad"] = len(essentials.threeAdrCode) 




def p_elseifexpression(p):
	'''elseifexpression :  ELSEIF LPAREN boolexpression RPAREN m statement_list3 n m elseifexpression
	| ELSE statement_list3
	| 
	| error'''

	p[0] = { }
	if len(p) == 10:
		essentials.backPatch(p[3]["trueList"] , p[5]["quad"] )
 		essentials.backPatch(p[3]["falseList"], p[8]["quad"] )
 		p[0]["nextList"] = essentials.mergeList(p[6]["nextList"], p[7]["nextList"])
 		p[0]["nextList"] = essentials.mergeList(p[0]["nextList"], p[9]["nextList"])
 		p[0]["breakList"] = p[6]["breakList"] + p[9]["breakList"]
 		p[0]["continueList"] = p[6]["continueList"] + p[9]["continueList"]
 	elif len(p) == 3 :
 		p[0]["nextList"] = p[2]["nextList"]
 		p[0]["nextList"] = p[2]["nextList"]
 		p[0]["breakList"] = p[2]["breakList"]
 		p[0]["continueList"] = p[2]["continueList"]
 	elif len(p) == 1:
 		p[0]["nextList"] = []
 		p[0]["breakList"] = []
 		p[0]["continueList"] = []

	##print "elseifexpression -> "  + p[0]
	inclLineNo()


def p_n(p):
	'''n :
	'''
	p[0] = { }
	p[0]["nextList"] = [ len(essentials.threeAdrCode)]
 	essentials.AddToThree(["goto"])


def p_for_statement(p):
	''' for_statement : FOR lvl LPAREN expression_forinit m boolexpression_state m expression_incl nm RPAREN statement_list3
	| FOR lvl LPAREN expression_forinit m boolexpression_state m RPAREN statement_list3
	| error'''
	
	global loopinglvl
	loopinglvl -=1

	p[0] = { }
	if len(p)==12:
		essentials.backPatch(p[11]["nextList"],p[7]["quad"])
		essentials.backPatch(p[9]["nextList"],p[5]["quad"])
		essentials.backPatch(p[6]["trueList"],p[9]["quad"])
		p[0]["nextList"] = p[6]["falseList"] + p[11]["breakList"]
		essentials.backPatch(p[11]["continueList"],p[7]["quad"])
		p[0]["continueList"] = [ ]
		p[0]["breakList"] = [ ]
		essentials.AddToThree(["goto",str(p[7]["quad"])])
	elif len(p)==10:
		essentials.backPatch(p[9]["nextList"],p[5]["quad"])
		essentials.backPatch(p[6]["trueList"],p[7]["quad"])
		p[0]["nextList"] = p[6]["falseList"] + p[9]["breakList"]
		essentials.backPatch(p[9]["continueList"],p[5]["quad"])
		p[0]["continueList"] = [ ]
		p[0]["breakList"] = [ ]
		essentials.AddToThree(["goto",str(p[5]["quad"])])
	##print "for_statement -> " + p[0]
	inclLineNo()

def p_lvl(p):
	'''lvl : '''
	global loopinglvl
	loopinglvl += 1

def p_while_statement(p):
	''' while_statement : WHILE lvl m LPAREN boolexpression RPAREN m statement_list3 
	| error'''

	global loopinglvl
	loopinglvl -=1

	p[0] = { }
	if(len(p)==9):
		essentials.backPatch(p[8]["nextList"],p[3]["quad"])
		essentials.backPatch(p[5]["trueList"],p[7]["quad"])
		p[0]["nextList"] = p[5]["falseList"] + p[8]["breakList"]
		p[0]["breakList"] = [ ]
		essentials.backPatch(p[8]["continueList"],p[3]["quad"])
		p[0]["continueList"] = [ ]	
		essentials.AddToThree(["goto",str(p[3]["quad"])]) 
	##print "while_statement ->" +p[0]
	inclLineNo()

def p_statement_list3(p):
	'''statement_list3 : statement
	| LBRACE statement_list2 RBRACE
	| error '''
	p[0] = { }
	if(len(p)==2):
		p[0]["nextList"] = p[1]["nextList"]
		p[0]["breakList"] = p[1]["breakList"]
		p[0]["continueList"] = p[1]["continueList"]	
	elif(len(p)==4):
		p[0]["nextList"] = p[2]["nextList"]
		p[0]["breakList"] = p[2]["breakList"]
		p[0]["continueList"] = p[2]["continueList"]

	##print "statement_list3 -> "  + p[0]
	inclLineNo()

def p_expression_forinit(p):
	'''expression_forinit : expression SEPARATOR expression_forinit
	 | expression_state 
	 | error'''
	p[0] = { }
	p[0]["nextList"] = [ ]
	##print "expression_forinit -> " + p[0]
	inclLineNo()

def p_expression_incl(p):
	'''expression_incl : expression SEPARATOR expression_incl 
	| expression 
	| error '''
	p[0] = { }
	try:
		p[0]["nextList"] = p[3]["nextList"]
	except:
		p[0]["nextList"] = [ ]
	##print "expression_forincl -> " + p[0]
	inclLineNo()

def p_function_statement(p):
	'''function_statement : FUNCTION IDENTIFIER_OTHER mark LPAREN argument RPAREN LBRACE statement_list2 RBRACE
	| FUNCTION IDENTIFIER_OTHER mark LPAREN RPAREN LBRACE statement_list2 RBRACE
	| FUNCTION IDENTIFIER_OTHER mark LPAREN argument RPAREN LBRACE RBRACE
	| FUNCTION IDENTIFIER_OTHER mark LPAREN RPAREN LBRACE RBRACE
	| error'''

	p[0] = { }
	# print p[3]["nextList"]

	name = p[2]
	if insideClass:
		name = name + "_" + p[2]
	p[0]["lexeme"] = p[2]

	p[0]["nextList"] = p[3]["nextList"]
	if len(p)==9:
		if p[5]==")":
			symTable.createSym(p[2],True , { "#attributes":0 })
		else:
			symTable.createSym(p[2],True , { "#attributes":p[5]["len"]})
		p[0]["nextList"] = p[0]["nextList"] + p[7]["nextList"]
	elif len(p)==10:
		symTable.createSym(p[2],True , {"#attributes":p[5]["len"]})
		p[0]["nextList"] = p[0]["nextList"] + p[8]["nextList"]
	else:
		symTable.createSym(p[2],True , { "#attributes":0 })

	essentials.threeAdrCode[p[3]["line"]].append("label")
	essentials.threeAdrCode[p[3]["line"]].append(name)

	# essentials.AddToThree(["ret"])
	Hash = symTable.get_Hash_Table()
	print "\nSymbol Table for " + p[2] + " function"
	print Hash
	symTable.end_scope()
	##print "function_statement -> " + p[0]
	global lvl
	lvl -= 1
	inclLineNo()

def p_mark(p):
	'''mark : 
	'''
	global lvl
	lvl += 1
	p[0] = { }
	symTable.begin_scope()
	p[0]["nextList"] = [ len(essentials.threeAdrCode) ]
	essentials.AddToThree(["goto"])
	p[0]["line"] = len(essentials.threeAdrCode)
	essentials.AddToThree([])
	if insideClass:
		essentials.AddToThree(["pop"])
		symTable.createSym("$this",False)
		essentials.AddToThree(["stack_pos","$this","3"])


def p_argument(p):
	'''argument : argument SEPARATOR IDENTIFIER_VARIABLE
	| IDENTIFIER_VARIABLE
	| error'''

	p[0] = { }
	p[0]["len"] = 1
	x = 2
	if insideClass:
		x = 3
	# print lvl
	if(len(p)>2):
		p[0]["len"] += p[1]["len"]
		symTable.createSym(p[3],False)
		essentials.AddToThree(["stack_pos",p[3],str(p[0]["len"]+x)])
	else:
		symTable.createSym(p[1],False)
		essentials.AddToThree(["stack_pos",p[1],str(p[0]["len"]+x)])

	##print "argument -> " + p[0]
	inclLineNo()

def p_function_call_statement(p):
	'''function_call_statement : IDENTIFIER_OTHER LPAREN arg_val RPAREN
	| IDENTIFIER_OTHER LPAREN RPAREN
	| error'''

	v = [ ]
	p[0] = { }
	# print "cc" +str(lvl)
	global functiondefinition

	if not curflag :
		if len(p)==5:
			functiondefinition.append([p[1],p[3]["len"]])
		else:
			functiondefinition.append([p[1],0])


	if lvl>0:
		# print "hi"
		for item,k in symTable.symbol_table.Hash.iteritems():	
			# print "gg" + item
			essentials.AddToThree(["push",item])
			v.append(item)
		for i in range(essentials.maxcounter):
			essentials.AddToThree(["push","var"+str(i)])
			v.append("var"+str(i))
	
	if len(p)==5:
		p[3]["vars"].reverse()
		for lexeme in p[3]["vars"]:
			# v.append(lexeme)
			essentials.AddToThree(["push",lexeme])
	if curflag:
		essentials.AddToThree(["push",curlexeme])
	
	essentials.AddToThree(["push","0"])
	if curflag:
		essentials.AddToThree(["=","classname",curlexeme+"[0]"])
		if p[1] not in HashDictClass:
			print "error: function not in class"
		else:
			essentials.AddToThree(["=","classvar",HashDictClass[p[1]]])
		classflag = 0
		if curlexeme == '$this':
			classflag = 1	
		essentials.AddToThree(["=","classflag",str(classflag)])
		essentials.AddToThree(["call","classFunction"])			
	else:	
		essentials.AddToThree(["call",p[1]])


	p[0]["lexeme"] = essentials.getNewTemp()

	if lvl==0:
		p[0]["lexeme"] = "gl_" + p[0]["lexeme"]
	essentials.AddToThree(["pop",p[0]["lexeme"]])

	if curflag :
			essentials.AddToThree(["pop"])

	if len(p)==5:		
		for lexeme in p[3]["vars"]:
			essentials.AddToThree(["pop"])
			
	v.reverse()
	for lexeme in v:
		essentials.AddToThree(["pop",lexeme])
	##print "function_call_statement -> " + p[0]
	inclLineNo()
	

def p_arg_val(p):
	'''arg_val : expression SEPARATOR arg_val
	| expression 
	| error'''

	p[0] = { }
	p[0]["len"] = 1
	p[0]["vars"] = [ p[1]["lexeme"] ]
	if len(p)==4:
		p[0]["vars"] = p[0]["vars"] + p[3]["vars"] 
		p[0]["len"] += p[3]["len"]
	##print "arg_val -> " + p[0]
	inclLineNo()


def p_datatype(p):
	'''datatype : INTEGER
	| FLOAT 
	| STRING '''

	p[0] = { }
	p[0]["lexeme"] = str(p[1])
	##print "datatype -> " + p[0]
	inclLineNo()



def p_array_declaration(p):
	'''array_declaration : IDENTIFIER_VARIABLE ASSIGN array_declaration
	| array_is
	| error'''

	p[0]={}
	if(len(p)==2):
		p[0]=p[1]
	elif(len(p)==4) :
		if lvl==0:
			p[1] = "gl_"+p[1]
		p[0]=p[3]
		symTable.createSym(p[1],False,{"IsArray":True})
		essentials.AddToThree(["new_array",p[1],p[3]["len"]])
		for i in range(0,len(p[3]["vars"])):
			essentials.AddToThree(["=",p[1]+"["+str(i)+"]",p[3]["vars"][i]])			





def p_array_is(p):
	'''array_is : ARRAY LPAREN arg_val RPAREN
	| ARRAY LPAREN RPAREN
	| error'''
	p[0]= {}
	if(len(p)==4):
		print "Error : empty array not allowed"
	elif(len(p)==5):	
		p[0]=p[3]

	##print "array_declaration -> " + p[0]
	inclLineNo()


def p_boolexpression_state(p):
	'''boolexpression_state : boolexpression SEMICOLON 
	| SEMICOLON
	| error
	'''
	p[0] = { }
	try:
		p[0]["nextList"] = p[1]["nextList"]
	except:
		p[0]["nextList"] = [ ]
	try:
		p[0]["trueList"] = p[1]["trueList"]
	except:
		p[0]["trueList"] = [ ]
	try:
		p[0]["falseList"] = p[1]["falseList"]
	except:
		p[0]["falseList"] = [ ]


def p_expression_state(p):
	''' expression_state : expression SEMICOLON 
	| SEMICOLON
	| error'''
	p[0] = { }
	p[0]["nextList"] = [ ]
	# if(len(p)==3) :
	# 		p[0]["lexeme"] = ""
	##print "expression_state -> " + p[0]
	inclLineNo()



def p_boolexpression(p):
	'''boolexpression : IDENTIFIER_VARIABLE ASSIGN boolexpression
	| indexed_identifier ASSIGN boolexpression
	| NOT boolexpression
	| LPAREN boolexpression RPAREN
	| boolexpression OR m boolexpression
	| boolexpression AND m boolexpression
	| expression EQUAL expression
	| expression NOTEQUAL expression
	| expression GREATER expression
	| expression GREATEREQ expression
	| expression LESSER expression
	| expression LESSEREQ expression
	| expression
	| BOOL_TRUE
	| BOOL_FALSE
	'''
	p[0] = { }
	if len(p)==2:
		if p[1]=="TRUE" or p[1]=="true":
			p[0]["trueList"] = [ len(essentials.threeAdrCode) ]
			p[0]["falseList"] = [ ]
			essentials.AddToThree(["goto"])
		elif p[1]=="FALSE" or p[1]=="false":
			p[0]["trueList"] = [ ]
			p[0]["falseList"] = [ len(essentials.threeAdrCode) ]
			essentials.AddToThree(["goto"])
		else:
			x = len(essentials.threeAdrCode)
			p[0]["trueList"] = [ x ]
			essentials.AddToThree(["ifgoto","!=",p[1]["lexeme"],"0"])
			p[0]["falseList"] = [ x+1 ]
			essentials.AddToThree(["goto"])
	if len(p)>1 and p[1]=="!":
		p[0]["trueList"] = p[2]["falseList"]
		p[0]["falseList"] = p[2]["trueList"]
	if len(p)>2 and p[2]=="or":
		essentials.backPatch(p[1]["falseList"], p[3]["quad"]);
		p[0]["trueList"] = essentials.mergeList(p[1]["trueList"],p[4]["trueList"])
		p[0]["falseList"] = p[4]["falseList"]
	if len(p)>2 and p[2]=="and":
		essentials.backPatch(p[1]["trueList"], p[3]["quad"]);
		p[0]["falseList"] = essentials.mergeList(p[1]["falseList"],p[4]["falseList"])
		p[0]["trueList"] = p[4]["trueList"]
	if len(p)>1 and p[1] == "(":
		p[0]["trueList"] = p[2]["trueList"]
		p[0]["falseList"] = p[2]["falseList"]
	if (len(p)>2) and ( p[2]=="==" or p[2]=="!=" or p[2]==">" or p[2]=="<" or p[2]=="<=" or p[2]==">=" ):
		p[0]["trueList"] = [ len(essentials.threeAdrCode) ]
		p[0]["falseList"] =  [ len(essentials.threeAdrCode) +1 ]
		essentials.AddToThree(["ifgoto",p[2],p[1]["lexeme"],p[3]["lexeme"]])
		essentials.AddToThree(["goto"]) 
	if len(p)>2 and p[2]=="=":
		if isinstance(p[1],basestring):
			if lvl == 0 :
				p[1] = "gl_" + p[1]
			x = p[1]
			p[1] = { }
			p[1]["lexeme"] = x
			symTable.createSym(p[1]["lexeme"],False)
		essentials.backPatch(p[3]["trueList"],len(essentials.threeAdrCode) )
		essentials.AddToThree(["=",p[1]["lexeme"],"1"])
		p[0]["nextList"] =[len(essentials.threeAdrCode)]
		essentials.AddToThree(["goto"]) 
		essentials.backPatch(p[3]["falseList"],len(essentials.threeAdrCode) )
		essentials.AddToThree(["=",p[1]["lexeme"],"0"])		
		p[0]["trueList"] = p[0]["falseList"] = [  ]


def p_expression(p):
	'''expression : IDENTIFIER_VARIABLE ASSIGN expression
	| indexed_identifier ASSIGN expression
	| classterm ASSIGN expression
	| expression PLUS expression
	| expression MINUS expression
	| expression MULT expression
	| expression DIVIDE expression
	| expression MOD expression
	| expression POWER expression
	| IDENTIFIER_VARIABLE PLUS2 expression
	| IDENTIFIER_VARIABLE MINUS2 expression
	| IDENTIFIER_VARIABLE MULT2 expression
	| IDENTIFIER_VARIABLE DIVIDE2 expression
	| IDENTIFIER_VARIABLE MOD2 expression
	| LPAREN expression RPAREN
	| indexed_identifier
	| term 
	| classterm
	| classfunctioncall
	| function_call_statement
	| error
	'''
	
	p[0] = { }
	try:
		if len(p)==4:
			if(p[2]=="="):
				if isinstance(p[1], basestring):
					if lvl==0:
						p[1] = "gl_" + p[1]
					p[0]["lexeme"] = p[1]
					symTable.createSym(p[0]["lexeme"],False)
					# print p[3]["lexeme"]
					essentials.AddToThree(["=",p[1],p[3]["lexeme"]])
				else:
					p[0]["lexeme"] = p[1]["lexeme"]
					essentials.AddToThree(["=",p[1]["lexeme"],p[3]["lexeme"]])
				p[0]["nextList"] = []
			else:
				if p[1]=="(":
					p[0] = p[2]
				else:
					if isinstance(p[1],basestring):
						if lvl==0:
							p[1] = "gl_"+p[1]
						if p[2]=="+=":
							essentials.AddToThree(["+",p[1],p[1],p[3]["lexeme"]])
						elif p[2]=="-=":
							essentials.AddToThree(["-",p[1],p[1],p[3]["lexeme"]])
						elif p[2]=="*=":
							essentials.AddToThree(["*",p[1],p[1],p[3]["lexeme"]])
						elif p[2]=="/=":
							essentials.AddToThree(["/",p[1],p[1],p[3]["lexeme"]])
						elif p[2]=="%=":
							essentials.AddToThree(["%",p[1],p[1],p[3]["lexeme"]])
						symTable.createSym(p[1],False)
						p[0]["nextList"] = []
					else:
						p[0]["lexeme"] = essentials.getNewTemp()
						if lvl==0:
							p[0]["lexeme"] = "gl_"+p[0]["lexeme"]
						essentials.AddToThree([p[2],p[0]["lexeme"],p[1]["lexeme"],p[3]["lexeme"]])
					try:
						p[0]["nextList"] = p[3]["nextList"]
					except:
						p[0]["nextList"] = [ ]
		elif len(p)==2:
			p[0] = p[1]
	except:
		'A'		
	##print "expression -> " + p[0]
	inclLineNo()
	#print p.slice

def p_m(p):
	''' m : 
	'''
	p[0] = { }
	p[0]["quad"] = len(essentials.threeAdrCode) 


def p_indexed_identifier(p):
	'''indexed_identifier : IDENTIFIER_VARIABLE LSQBRACKET expression RSQBRACKET
	|	PLUS IDENTIFIER_VARIABLE LSQBRACKET expression RSQBRACKET
	|	MINUS IDENTIFIER_VARIABLE LSQBRACKET expression RSQBRACKET
	'''
	p[0] = { }
	if(len(p)==5):
		if lvl == 0:
			p[1] = "gl_" + p[1]
		p[0]["lexeme"] = p[1] + "[" + p[3]["lexeme"] + "]"
	else:
		if lvl == 0:
			p[2] = "gl_" + p[2]
		p[0]["lexeme"] = p[1] + p[2] + "[" + p[4]["lexeme"] + "]"
	p[0]["nextList"] = p[0]["trueList"] = p[0]["falseList"] = [ ]
	inclLineNo()

def p_sqbracket(p):
	'''sqbracket : LSQBRACKET INTEGER RSQBRACKET sqbracket 
	| LSQBRACKET IDENTIFIER_VARIABLE RSQBRACKET sqbracket
	| LSQBRACKET INTEGER RSQBRACKET 
	| LSQBRACKET IDENTIFIER_VARIABLE RSQBRACKET 
	| error'''
	if len(p) == 5:
		p[0] = p[1] + str(p[2]) + p[3] + p[4]
	elif len(p) == 4 :
		p[0] = p[1] + str(p[2]) + p[3]
	else:
		p[0] = p[1]

	##print "sqbracket -> " + p[0]
	inclLineNo()
	#print p.slice


def p_term(p):
	'''term : IDENTIFIER_VARIABLE
	| datatype
	| PLUS IDENTIFIER_VARIABLE
	| PLUS datatype
	| MINUS IDENTIFIER_VARIABLE
	| MINUS datatype
	| error'''

	p[0] = { }
	if len(p)==2:
		if isinstance(p[1],basestring):
			if lvl==0:
				p[1] = "gl_" + p[1]
			p[0]["lexeme"] = p[1]
			symTable.createSym(p[1],False)
		else:
			p[0]["lexeme"] = p[1]["lexeme"]
		p[0]["nextList"] = p[0]["falseList"] = p[0]["trueList"] = [ ]
	else:	
		p[0]["lexeme"] = essentials.getNewTemp()	
		if lvl==0:
			p[2] = "gl_" + p[2]
			p[0]["lexeme"] = "gl_" + p[0]["lexeme"]
		attr = [ "=" , p[0]["lexeme"] , p[1] + str(p[2]) ]
		essentials.AddToThree(attr)
		p[0]["nextList"] = essentials.makeList()
		p[0]["falseList"] = p[0]["trueList"] = [ ]
	# print "gg",p[0]
	##print "term -> " + p[0]
	inclLineNo()
	#print p.slice



def p_error(p):
	if p != None:
		print "line :",p.lineno,"-Parsing Error Found at Token:",p.type
		parser.errok()

# def p_error(p):
#     print "line :",p.lineno,"-Parsing Error Found at Token:",p.type
#     # Read ahead looking for a closing '}'
#     while 1:
#         tok = parse.token()             # Get the next token
#         if not tok or tok.type == 'RBRACE' or tok.type == END or tok.type == SEMICOLON: break
#     parser.restart()
#     return tok

old_stderr = sys.stderr



# lexer = lex.lex()
parser = yacc.yacc()
# lexer.input(data)
sys.stderr = open("temp.txt" , 'w')
result = parser.parse(data , debug = 1)
sys.stderr = old_stderr
# make_graph.derivation(filename)
# result = parser.parse(data)

print "\n3 Address Code"

for i in range(len(essentials.threeAdrCode)):
	print str(i)+",",
	for j in range(len(essentials.threeAdrCode[i])-1):
		print essentials.threeAdrCode[i][j]+",",
	print essentials.threeAdrCode[i][len(essentials.threeAdrCode[i])-1]