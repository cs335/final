# Reserved Keywords
Keywords = {
   'if'        : 'IF',
   'else'      : 'ELSE',
   'elseif'    : 'ELSEIF',
   'for'       : 'FOR',
   'while'     : 'WHILE',
   'foreach'   : 'FOREACH',
   'as'        : 'AS',
   'switch'    : 'SWITCH',
   'case'      : 'CASE',
   'break'     : 'BREAK',
   'continue'  : 'CONTINUE',
   'return'    : 'RETURN',
   'function'  : 'FUNCTION',
   'echo'      : 'ECHO',
   'array'     : 'ARRAY',
   'default'   : 'DEFAULT',
   'class'     : 'CLASS',
   'define'    : 'DEFINE',
   'true'      : 'BOOL_TRUE',
   'TRUE'      : 'BOOL_TRUE',
   'false'     : 'BOOL_FALSE',
   'FALSE'     : 'BOOL_FALSE',
   'public'    : 'PUBLIC',
   'private'   : 'PRIVATE',
   'var'       : 'VAR',
   'global'    : 'GLOBAL',
   'fscanf'    : 'SCAN',
   'STDIN'     : 'INPUT', 
   'new'       : 'NEW',
   '$this'     : 'THIS'
}