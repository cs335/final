#! /usr/bin/python

import ply.lex as lex
import keywords 
import operators

Keywords = keywords.Keywords
Operators = operators.Operators
Reserved = Keywords.copy()
Reserved.update(Operators)


# List of token names.  
tokens = [
   'START',
   'END',
   'IDENTIFIER_VARIABLE',
   'IDENTIFIER_OTHER',
   'OPERATOR',
   'INTEGER',
   'FLOAT',
   'STRING',
   'SEMICOLON',
   'COLON',
   'LPAREN',
   'RPAREN',
   'LBRACE',
   'RBRACE',
   'LSQBRACKET',
   'RSQBRACKET',
   'BLOCKCOMMENTS',
   'SINGLELINECOMMENTS',
   'ILLEGALCHARACTER'
] +  list(Reserved.values()) 


# Regular expression rules for simple tokens
t_SEMICOLON = r'\;'
t_COLON = r'\:'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LBRACE  = r'\{'
t_RBRACE  = r'\}'
t_LSQBRACKET = r'\['
t_RSQBRACKET = r'\]'



# A regular expression rule with some action code

#Start and End of php code
def t_START(t):
    r'\<\?\p\h\p'
    return t

def t_END(t):
    r'\?\>'
    return t


#Illegal Characters
# def t_ILLEGALIDENTIFIER(t):
#   r'(\$\d[a-zA-Z_0-9]*) | (\d+[a-zA-Z_]+[a-zA-Z_0-9]*) '
#   return t


#Comments
def t_SINGLELINECOMMENTS(t):
    r'(\/\/[^\n]*) | (\#[^\n]*) '
    #r'^(//)[^\n\r]*[\n\r]'
    return t

def t_BLOCKCOMMENTS(t):
    r'\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/'
    return t


# identifiers for variables and functions
# def t_IDENTIFIER_ARRAY(t):
#     r'(\$[a-zA-Z_][a-zA-Z_0-9]*\[\d+\]) | (\$[a-zA-Z_][a-zA-Z_0-9]*\[\$[a-zA-Z_][a-zA-Z_0-9]*\]) '
#     return t


def t_IDENTIFIER_VARIABLE(t):
    r'\$[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = Reserved.get(t.value,'IDENTIFIER_VARIABLE')    # Check for reserved words
    return t

def t_IDENTIFIER_OTHER(t):
    #r'[a-zA-Z_][a-zA-Z_0-9]*'
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = Reserved.get(t.value,'IDENTIFIER_OTHER')    # Check for reserved words
    return t


#Data Types
def t_FLOAT(t):
    #r'\d+.\d+'
    r'[+-]?(\d+(\.\d+)|\.\d+)'
    try:
         t.value = float(t.value)    
    except ValueError:
         print "Line %d: Number %s is too large!" % (t.lineno,t.value)
   
    return t

def t_INTEGER(t):
    # r'[+-]?\d+'
    r'\d+'
    try:
         t.value = int(t.value)    
    except ValueError:
         print "Line %d: Number %s is too large!" % (t.lineno,t.value)
	 t.value = 0
    return t



def t_STRING(t):
 # r'[\A\"](.*?(\\\")*?.*?)*?[\Z\"]'
  r'("(\\"|[^"])*")|(\'(\\\'|[^\'])*\')'
  #r'\"([^\\\n]|(\\.))*?\"'
  return t


#Operators

def t_PLUS2(t):
     r'\+\='
     return t

def t_MINUS2(t):
     r'\-\='
     return t

def t_MULT2(t):
      r'\*\='
      return t

def t_DIVIDE2(t):
      r'\\\='
      return t


def t_MOD2(t):
     r'\%\='
     return t

def t_EQUALTYPE(t):
      r'\=\=\='
      return t


def t_EQUAL(t):
      r'\=\='
      return t

def t_POWER(t):
    r'\*\*'
    return t





def t_INC(t):
     r'\+\+'
     return t

def t_DEC(t):
     r'\-\-'
     return t

def t_NOTEQUAL2(t):
      r'\<\>'
      return t


def t_GREATEREQ(t):
      r'\>\='
      return t

def t_LESSEREQ(t):
      r'\<\='
      return t


def t_KEY_VALUE(t):
      r'\=\>'
      return t

def t_AND(t):
      r'and'
      return t

def t_OR(t):
      r'or'
      return t

def t_XOR(t):
      r'xor'
      return t

def t_AND2(t):
      r'\&\&'
      return t

def t_OR2(t):
      r'\|\|'
      return t

def t_CONCAT2(t):
      r'\.\='
      return t


def t_CONCAT(t):
      r'\.'
      return t


def t_SEPARATOR(t):
      r'\,'
      return t

def t_ARROW(t):
      r'\-\>'
      return t


def t_NOTEQUAL(t):
      r'\!\='
      return t



def t_NOTEQUALTYPE(t):
      r'\!\=\='
      return t

def t_NOT(t):
      r'\!'
      return t

def t_ASSIGN(t):
     r'\='
     return t


def t_PLUS(t):
     r'\+'
     return t

def t_MINUS(t):
     r'\-'
     return t

def t_MULT(t):
      r'\*'
      return t

def t_DIVIDE(t):
      r'\\'
      return t


def t_MOD(t):
     r'\%'
     return t

def t_GREATER(t):
      r'\>'
      return t

def t_LESSER(t):
      r'\<'
      return t


def t_OPERATOR(t):
     #r'([\/][\!\%\^\&\*\+\|\-\=\<\>]+) | ([\!\%\^\&\*\+\|\-\=\<\>]+)'
     r'[\!\%\^\&\*\+\|\-\=\<\>\/\.\,]+'
     t.type = Reserved.get(t.value,'ILLEGALCHARACTER')
     return t 

#Keep Track of new line
def t_newline(t):
    r'[\r\n]+'
    t.lexer.lineno += len(t.value)


# def t_ILLEGALCHARACTER(t):
#   # r'([\!\%\^\&\*\+\|\-\=\<\>\/\.\,\@]+\$*[a-zA-Z_][a-zA-Z_0-9]*) '
#   r''
#   return t

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t' 



# Error handling rule
def t_error(t):
    # print "Illegal character '%s'" % t.value[0]
    print "Illegal syntax '%s'" % t.value[0]
    t.lexer.skip(1)
    pass
    # return t

# Build the lexer
lexer = lex.lex(errorlog=lex.NullLogger())


# data = raw_input() + "\n"
# c = raw_input()

# try:
#   while  c is not None:
#      data = data + c + "\n"
#      c = raw_input()
# except EOFError:
#       pass

# lex.input(data)
# dictionary = {}

# for c in tokens:
#    dictionary[c] = []
# # Tokenize
# while 1:
#    tok = lex.token()
#    if not tok: break      # No more input
#    dictionary[tok.type].append(tok.value);
   

# for c,v in dictionary.iteritems():
#   if len(v)>0:
#    c = c + "\t" + str(len(v)) + "\n"
#    for u in set(v):
#     c =  c + str(u) + "\n"
#    print c[:len(c)-1] + "\n"
