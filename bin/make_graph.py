#! /usr/bin/python
import re

def clean(word) :
	return { 
    '&': "ampersant",
    '(': "open_bracket",
	')': "close_bracket",
	'*': "star",
	'+': "plus",
	',': "comma",
	'-': "hyphen",
	'.': "dot",
	'/': "forward_slash",
	':': "colon",
	';': "semi_colon",
	'<': "less",
	'=': "equal",
	'>': "more",
    "<empty>" : "empty",
	}.get(word,word)

def derivation(file_name) :
	outfile = open("bin/" + file_name.split('.')[0] + ".txt", 'w')	
	f = open("temp.txt",'r')
	a = re.compile("Action : Reduce rule")
	
	for line in reversed(f.readlines()):
	    if (a.match(line)):
			line = line.split('[')[1].split(']')[0]
			lhs = line.split("->")
			rhs = lhs[1].split(" ")
			# print lhs[0]
			outfile.write(lhs[0]+"\n")
			for b in rhs:
				# print b
				outfile.write(b+"\n")
			# print 0
			outfile.write("0"+"\n")	
	# print -1
	outfile.write("-1"+"\n")
	return