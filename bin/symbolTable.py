#!/usr/bin/env python

#Symbol Table has been designed for data-types, Identifiers and procedures 

#Class Table is a standard symbol table
# It is a dictionary of dictionaries.

#Width for standard data types
import essentials
width= {'INT':4, 'FLOAT':8, 'CHAR':1, 'BOOL':4} #8- bit characters

class Table:
	def __init__(self, global_SymbolTable = None,prev = None):
		self.Hash = { }
		self.prev_table = prev
		if global_SymbolTable is not None:
			self.global_SymbolTable = global_SymbolTable
		else:
			self.global_SymbolTable = self
		self.maxcounter = 0

	def createSym(self,name,Isfunc,Attribute_List):
		if Isfunc:
			if name in self.global_SymbolTable.Hash:
				print "Error"
			else:
				self.global_SymbolTable.Hash[name] = { }
				self.global_SymbolTable.Hash[name]["Isfunc"] = True
				for item in Attribute_List :
					self.global_SymbolTable.Hash[name][item] = Attribute_List[item]
				# self.global_SymbolTable[name][Isfunc] = True
		else:
			if name not in self.Hash:
				self.Hash[name] = { }
				self.Hash[name]["Isfunc"] = False
			for item in Attribute_List :
				self.Hash[name][item] = Attribute_List[item]

	def updateSym(self,name,Attribute_List,Isfunc,Attribute_name,value):
		if Isfunc :
			print "Error"
		else:
			ASSERT(self.Hash.has_key(name))
			self.Hash[name][Attribute_name] = value

	def get_Attribute_Value(self, name , Attribute_Name): 
		try :
			return self.Hash[name][Attribute_Name]
		except :
			return None

	def updateSymGlobal(self,name,Attribute_List,Isfunc,Attribute_name,value):
		if Isfunc :
			print "Error"
		else:
			ASSERT(self.global_SymbolTable.Hash.has_key(name))
			self.global_SymbolTable.Hash[name][Attribute_name] = value

	def get_Attribute_ValueGlobal(self, name , Attribute_Name , Isfunc):

		try :
			return self.global_SymbolTable.Hash[name][Attribute_Name]
		except :
			return None


	def get_Hash_Table(self):
		return self.Hash




class SymbolTable:
	def __init__(self):
		self.symbol_table = Table()

	def createSym(self, name ,Isfunc ,Attribute_List={}):
		# print name
		self.symbol_table.createSym(name , Isfunc, Attribute_List)

	def updateSym(self, name ,Isfunc,Value, Attribute_Name): # returns a boolean if it was a successful update
		self.symbol_table.updateSym(name, Isfunc, Attribute_Name , Value)

	def get_Attribute_Value(self , name , Attribute_Name ):
		return self.symbol_table.get_Attribute_Value(name , Attribute_Name)

	def get_Attribute_ValueGlobal(self , name , Attribute_Name , Isfunc):
		return self.symbol_table.get_Attribute_Value(name , Attribute_Name,Isfunc)

	def get_Hash_Table(self):
		return self.symbol_table.get_Hash_Table()

	def begin_scope(self):
		new_table = Table(self.symbol_table.global_SymbolTable,self.symbol_table)
		self.symbol_table = new_table
		return self.symbol_table


	def end_scope(self):
		self.symbol_table = self.symbol_table.prev_table
		essentials.maxcounter= self.symbol_table.maxcounter


