PLY: PARSE DEBUG START

State  : 0
Stack  : . LexToken(START,'<?php',1,0)
Action : Shift and goto state 3

State  : 3
Stack  : START . LexToken(CLASS,'class',2,7)
Action : Shift and goto state 17

State  : 17
Stack  : START CLASS . LexToken(IDENTIFIER_OTHER,'testclass',2,13)
Action : Shift and goto state 62

State  : 62
Stack  : START CLASS IDENTIFIER_OTHER . LexToken(LBRACE,'{',2,22)
Action : Reduce rule [class_name -> IDENTIFIER_OTHER] with ['testclass'] and goto state 27
Result : <dict @ 0x7f51d1e53e88> ({'lexeme': 'testclass'})

State  : 63
Stack  : START CLASS class_name . LexToken(LBRACE,'{',2,22)
Action : Reduce rule [mark1 -> <empty>] with [] and goto state 28
Result : <dict @ 0x7f51d1e55398> ({'classNextList': 1})

State  : 114
Stack  : START CLASS class_name mark1 . LexToken(LBRACE,'{',2,22)
Action : Shift and goto state 169

State  : 169
Stack  : START CLASS class_name mark1 LBRACE . LexToken(PUBLIC,'public',3,26)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$a',3,33)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',3,35)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(PUBLIC,'public',4,39)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$a',';'] and goto state 34
Result : <dict @ 0x7f51d1e53b40> ({'lexeme': ['$a', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars . LexToken(PUBLIC,'public',4,39)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE classvars PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$b',4,46)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE classvars PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',4,48)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE classvars PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(PUBLIC,'public',5,52)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$b',';'] and goto state 34
Result : <dict @ 0x7f51d1e556e0> ({'lexeme': ['$b', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars classvars . LexToken(PUBLIC,'public',5,52)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE classvars classvars PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$c',5,59)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE classvars classvars PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',5,61)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE classvars classvars PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(PUBLIC,'public',6,65)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$c',';'] and goto state 34
Result : <dict @ 0x7f51d1e55a28> ({'lexeme': ['$c', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars . LexToken(PUBLIC,'public',6,65)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$d',6,72)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',6,74)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(PUBLIC,'public',7,78)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$d',';'] and goto state 34
Result : <dict @ 0x7f51d1e55e88> ({'lexeme': ['$d', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars . LexToken(PUBLIC,'public',7,78)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$e',7,85)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',7,87)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(PUBLIC,'public',8,91)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$e',';'] and goto state 34
Result : <dict @ 0x7f51d1e5b168> ({'lexeme': ['$e', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars . LexToken(PUBLIC,'public',8,91)
Action : Shift and goto state 212

State  : 212
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars PUBLIC . LexToken(IDENTIFIER_VARIABLE,'$f',8,98)
Action : Shift and goto state 246

State  : 246
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE . LexToken(SEMICOLON,';',8,100)
Action : Shift and goto state 275

State  : 275
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars PUBLIC IDENTIFIER_VARIABLE SEMICOLON . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [classvars -> PUBLIC IDENTIFIER_VARIABLE SEMICOLON] with ['public','$f',';'] and goto state 34
Result : <dict @ 0x7f51d1e5b398> ({'lexeme': ['$f', 0]})

State  : 207
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars classvars . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> <empty>] with [] and goto state 31
Result : <dict @ 0x7f51d1e5b5c8> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e5b398>,<dict @ 0x7f51d1e5b5c8>] and goto state 29
Result : <dict @ 0x7f51d1e55d70> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e5b168>,<dict @ 0x7f51d1e55d70>] and goto state 29
Result : <dict @ 0x7f51d1e5b910> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e55e88>,<dict @ 0x7f51d1e5b910>] and goto state 29
Result : <dict @ 0x7f51d1e5b6e0> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars classvars classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e55a28>,<dict @ 0x7f51d1e5b6e0>] and goto state 29
Result : <dict @ 0x7f51d1e5b4b0> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e556e0>,<dict @ 0x7f51d1e5b4b0>] and goto state 29
Result : <dict @ 0x7f51d1e5b280> ({'nextList': []})

State  : 239
Stack  : START CLASS class_name mark1 LBRACE classvars class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Reduce rule [class_statementlist -> classvars class_statementlist] with [<dict @ 0x7f51d1e53b40>,<dict @ 0x7f51d1e5b280>] and goto state 29
Result : <dict @ 0x7f51d1e5b050> ({'nextList': []})

State  : 209
Stack  : START CLASS class_name mark1 LBRACE class_statementlist . LexToken(RBRACE,'}',10,104)
Action : Shift and goto state 243

State  : 243
Stack  : START CLASS class_name mark1 LBRACE class_statementlist RBRACE . LexToken(IDENTIFIER_VARIABLE,'$T',12,108)
Action : Reduce rule [class_statement -> CLASS class_name mark1 LBRACE class_statementlist RBRACE] with ['class',<dict @ 0x7f51d1e53e88>,<dict @ 0x7f51d1e55398>,'{',<dict @ 0x7f51d1e5b050>,'}'] and goto state 25
Traceback (most recent call last):
  File "./irgen.py", line 1190, in <module>
    result = parser.parse(data , debug = 1)
  File "/usr/lib/python2.7/dist-packages/ply/yacc.py", line 265, in parse
    return self.parsedebug(input,lexer,debug,tracking,tokenfunc)
  File "/usr/lib/python2.7/dist-packages/ply/yacc.py", line 425, in parsedebug
    p.callable(pslice)
  File "./irgen.py", line 186, in p_class_statement
    essentials.backPatchClass(p[3]["classNextList"],len(essentials.classthreeAddr))
  File "/home/sandipan/Desktop/sem6/CS335A/assignment/final/bin/essentials.py", line 25, in backPatchClass
    for i in range(len(nlist)) :
TypeError: object of type 'int' has no len()
