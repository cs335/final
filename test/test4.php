<?php


function factorize($b){
    function checkprime($a){
        for($i=2;$i*$i<=$a;$i+=1){
            if($a%$i==0) return 0;
        }
        return 1;
    };
    $curfac=1;
    for($i=2;$i<$b;$i+=1){
        if(checkprime($i)==0) continue;
        while($b%$i==0)
            $b/=$i;
        $curfac=$i;
        if($b==1) break;
    }
    if($b!=1) $curfac=1;
    return;
}
for($m=2;$m<100;$m+=1){
    factorize($m);
}
;
?>